//
//  GithubServiceTests.swift
//  PureTextAssignmentTests
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import XCTest
@testable import PureTextAssignment

class GithubServiceTests: XCTestCase {
    
    let service = GithubService()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearchByQuery() {
        
        let exp = self.expectation(description: "search")
        
        service.search(by: "swift").then { (repositories) -> Void in
            if repositories.count == 0 {
                XCTFail()
            } else {
                let repository = repositories[1]
                
                if repository.fullName.isEmpty {
                    XCTFail()
                }
                
                if let updatedAt = repository.updatedAt {
                    if updatedAt.isEmpty {
                        XCTFail()
                    }
                } else {
                    XCTFail()
                }
                
                guard let owner = repository.owner else { return XCTFail() }
                
                if owner.avatarURL == nil {
                    XCTFail()
                }
                
                if owner.name.isEmpty {
                    XCTFail()
                }
                
                exp.fulfill()
            }
        }.catch { (error) in
                print(error.localizedDescription)
                XCTFail()
        }
        
        self.waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                print(error.localizedDescription)
                XCTFail()
            }
        }
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
