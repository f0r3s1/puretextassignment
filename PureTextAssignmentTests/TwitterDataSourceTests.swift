//
//  TwitterDataSourceTests.swift
//  PureTextAssignmentTests
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import XCTest
@testable import PureTextAssignment
import AlamofireImage

class TwitterDataSourceTests: XCTestCase {
    
    var tableView = UITableView()
    var fakeTweets:[Tweet]?
    var twitterDataSource:TwitterDataSource?
        
    override func setUp() {
        super.setUp()
        
        var fakeTweets = [Tweet]()
        
        fakeTweets.append(Tweet.init(with: "Alalalala Ololololol", by: "ololo", and: "@blabla", image: "http://httpbin.org/image/png"))
        fakeTweets.append(Tweet.init(with: "Ololololol Alalalala", by: "blabla", and: "@ololo", image: "http://httpbin.org/image/png"))
        fakeTweets.append(Tweet.init(with: "GAGAGA LALALA", by: "blabla", and: "@ololo", image: "http://httpbin.org/image/svg"))
        
        self.fakeTweets = fakeTweets
        self.twitterDataSource = TwitterDataSource.init(with: self.fakeTweets!)

//        tableView.register(TweetTableViewCell.self, forCellReuseIdentifier: "tweet_cell")

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNumberOfRows() {
        let numberOfSections = self.twitterDataSource?.numberOfSections(in: self.tableView)
        XCTAssertEqual(numberOfSections, 1)
        
        let numberOfRows = self.twitterDataSource?.tableView(self.tableView, numberOfRowsInSection: 0)
        XCTAssertEqual(numberOfRows, self.fakeTweets?.count)
    }
    
    func testCellFilling() {
        let homeViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home_vc") as! HomeViewController
        homeViewController.loadView()
        
        let indexPath = IndexPath.init(row: 1, section: 0)
        let cell = self.twitterDataSource?.tableView(homeViewController.tableView, cellForRowAt: indexPath) as! TweetTableViewCell
        guard let tweet = self.fakeTweets?[indexPath.row] else { return XCTFail() }
        XCTAssertEqual(cell.bodyLabel.text, tweet.body)
        XCTAssertEqual(cell.authorLabel.text, "By \(tweet.author)")
    }
    
}
