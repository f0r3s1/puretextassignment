//
//  MapHelper.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import ObjectMapper
import PromiseKit

internal class MapHelper<Element: NSObject>  where Element: Mappable {
    
    static func mapArray(from data:Any?, and id:Int? = nil) -> Promise<[Element]> {
        guard let result = Mapper<Element>().mapArray(JSONObject: data) else { return Promise(value: [Element]()) }
        
        return Promise(value: result)
    }
    
    static func map(from data:Any?) -> Promise<Element> {
        guard let result = Mapper<Element>().map(JSONObject: data) else { return Promise(value: Element()) }
        return Promise(value: result)
    }
}
