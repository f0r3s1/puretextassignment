//
//  ExternalRepository.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import PromiseKit
import Alamofire

class ExternalRepository: NSObject {
    static func makeRequest(_ url:URL, method:HTTPMethod, parameters:Parameters?, headers:[String : String]? = nil) -> Promise<(Any, DataResponse<Any>)> {
        var requestHeaders = [String:String]()
        if let additionalHeaders = headers {
            for key in additionalHeaders.keys {
                requestHeaders[key] = additionalHeaders[key]
            }
        }
        
        return Promise(resolvers: { (fulfill, reject) in
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders).validate().responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    fulfill((data, response))
                case .failure(let err):
                    reject(NetworkErrorHandler.transform(err, with: response))
                }
            }
        })
    }
}
