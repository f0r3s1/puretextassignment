//
//  Tweet.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit

class Tweet: NSObject {
    var body = ""
    var author = ""
    var authorImageURL:URL?
    
    init(with body: String, by authorName: String, and authorUsername: String, image authorImageURLString: String ) {
        self.body = body
        self.author = "\(authorUsername), @\(authorUsername)"
        self.authorImageURL = URL(string: authorImageURLString)
    }
}
