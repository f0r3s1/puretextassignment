//
//  Repo.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import ObjectMapper

class Repo: NSObject, Mappable {
    var fullName = ""
    var updatedAt:String?
    var owner:RepoOwner?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        fullName <- map["full_name"]
        owner <- map["owner"]
        updatedAt <- map["updated_at"]
    }
}
