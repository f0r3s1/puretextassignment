//
//  RepoOwner.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import ObjectMapper

class RepoOwner: NSObject, Mappable {
    private var avatarURLString = ""
    var avatarURL:URL?
    var name = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["login"]
        avatarURLString <- map["avatar_url"]
        self.avatarURL = URL(string: self.avatarURLString)
    }
}
