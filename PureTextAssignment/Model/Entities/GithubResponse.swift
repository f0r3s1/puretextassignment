//
//  GithubResponse.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import ObjectMapper

class GithubResponse: NSObject, Mappable {
    var items:[Repo]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        items <- map["items"]
    }

}
