//
//  NetworkErrorHandler.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class NetworkErrorHandler: NSObject {
    
    static func transform(_ err:Error, with response:DataResponse<Any>) -> BackEndError {
        let error = BackEndError()
        error.response = response.description
        error.request = response.request?.url?.absoluteString ?? ""
        error.error = err
        guard let data = response.data else { return error }
        guard let jsonString = String(data: data, encoding: .utf8) else { return error }
        error.body = jsonString
        guard let message = BackEndError(JSONString: jsonString)?.message else { return error }
        error.message = message
        
        return error
    }
    
    func handle(_ error:BackEndError) -> HandlerTask {
        if let aferror = error.error as? AFError {
            if aferror.responseCode == 401 {
                return NetworkErrorHandler.handleUnauthorized()
            } else if error.message != "" {
                return HandlerTask(message: error.message)
            } else {
//                Logger().record(backEnd: error)
                return HandlerTask(message: aferror.localizedDescription)
            }
        } else {
            return HandlerTask(message: error.error?.localizedDescription ?? "Something went wrong. Please, try again", segue: nil)
        }
    }
    
    static func handleUnauthorized() -> HandlerTask {
        return HandlerTask(message: "Sorry, your session expired. Please, sign in again", controller: nil, segue: nil)
    }
}

class HandlerTask {
    var message:String
    var controller:String?
    var segue:String?
    
    init(message:String, controller:String? = nil, segue:String? = nil) {
        self.message = message
        self.controller = controller
        self.segue = segue
    }
}

class BackEndError: Mappable, Error {
    var error:Error?
    var message = ""
    var response = ""
    var request = ""
    var body = ""
    var cancelled = false
    
    init(message: String = "") {
        self.message = message
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
}

enum InternalError: Error {
    case unknown
    case lostData(reason:String)
    case memoryWarning
    
    func getErrorString() -> String {
        switch self {
        case .unknown:
            return "OOPS! Internal Error. Please, remember current time and contact us."
        case .lostData(reason: let reason):
            return "OOPS! Internal Error: \n\(reason).\n Please, remember current time and contact us."
        case .memoryWarning:
            return "Memory warning"
        }
    }
}
