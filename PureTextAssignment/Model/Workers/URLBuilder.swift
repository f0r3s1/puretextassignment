//
//  URLBuilder.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit

internal class URLBuilder: NSObject {
    
    static func getURL(path:String = "",
                       queryItems:[String:String]? = nil) -> URL? {
        return self.build(path: path, queryItems: queryItems)
    }
    
    static fileprivate func build(from scheme:String = "https",
                                  authority:String = "api.github.com",
                                  path:String = "",
                                  queryItems:[String:String]? = nil) -> URL? {
        
        var components = URLComponents()
        var url:URL?
        
        components.scheme = scheme
        components.host = authority
        components.path = path
        
        if let items = queryItems {
            var query = [URLQueryItem]()
            
            for item in items {
                query.append(URLQueryItem(name: item.key, value: item.value))
            }
            
            components.queryItems = query
        }
        
        do {
            url = try components.asURL()
            return url
        } catch {
            return nil
        }
    }
}
