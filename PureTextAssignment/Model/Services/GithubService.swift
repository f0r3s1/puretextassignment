//
//  GithubService.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import PromiseKit

class GithubService: NSObject {
    
    func search(by query:String) -> Promise<[Repo]> {
        guard let url = URLBuilder.getURL(path: "/search/repositories", queryItems: ["q" : query]) else { return Promise(error: InternalError.lostData(reason: "Invalid URL"))}
        return ExternalRepository.makeRequest(url, method: .get, parameters: nil).then(execute: { (data) -> Promise<[Repo]> in
            guard let result = MapHelper<GithubResponse>.map(from: data.0).value else { return Promise(error: InternalError.lostData(reason: "Unable to map data"))}
            guard let repos = result.items else { return Promise(error: InternalError.lostData(reason: "No repos found")) }
            return Promise(value: repos)
        })
    }
}
