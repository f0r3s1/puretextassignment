//
//  AuthViewController.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/15/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import Accounts
import Social

class AuthViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var swifter: Swifter?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.swifter = Swifter(consumerKey: "RErEmzj7ijDkJr60ayE2gjSHT", consumerSecret: "SbS0CHk11oJdALARa7NDik0nty4pXvAxdt7aj0R5y1gNzWaNEx")
        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - Helpers
extension AuthViewController {
    fileprivate func alert(title: String, message: String?, selector:Selector) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Try again", style: .default, handler: { (action) in
            self.perform(selector)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - Preparing for home
extension AuthViewController {
    @objc fileprivate func fetchTwitterHomeStream() {
        let failureHandler: (Error) -> Void = { error in
            self.alert(title: "Error", message: error.localizedDescription, selector: #selector(self.signIn))
        }
        
        self.swifter?.getHomeTimeline(count: 50, success: { json in
            // Successfully fetched timeline, so lets create and push the table view
            
            let tweetsViewController = self.storyboard!.instantiateViewController(withIdentifier: "home_vc") as! HomeViewController
            
            guard let tweets = self.map(json.array) else {
                self.alert(title: "Error", message: "No tweets available", selector: #selector(self.signIn))
                return
            }
            
            tweetsViewController.tweets = tweets
            self.navigationController?.pushViewController(tweetsViewController, animated: true)
            
        }, failure: failureHandler)
    }
    
    @objc fileprivate func signIn() {
        let failureHandler: (Error) -> Void = { error in
            self.alert(title: "Error", message: error.localizedDescription, selector: #selector(self.signIn))
        }
        
        let url = URL(string: "swifter://success")!
        self.swifter?.authorize(with: url, presentFrom: self, success: { _, _ in
            self.fetchTwitterHomeStream()
        }, failure: failureHandler)
    }
    
    private func map(_ tweets:[JSON]?) -> [Tweet]? {
        guard let tweetsJSON = tweets else { return nil }
        
        var result = [Tweet]()
        for tweet in tweetsJSON {
            let tweetEntity = Tweet.init(with: tweet["text"].string!, by: tweet["user"]["name"].string!, and: tweet["user"]["screen_name"].string!, image: tweet["user"]["profile_image_url_https"].string!)
            result.append(tweetEntity)
        }
        
        return result
    }
}

//MARK: - Actions
extension AuthViewController {
    @IBAction func onSignIn(_ sender: Any) {
        self.signIn()
    }
}
