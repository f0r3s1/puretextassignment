//
//  HomeViewController.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var twitterDataSource:TwitterDataSource?
    var githubDataSource:GithubDataSource?
    var tweets:[Tweet]? {
        didSet {
            if let tweets = self.tweets {
                self.twitterDataSource?.tweets = tweets
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupSelf()
        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - Content Filling
extension HomeViewController {
    fileprivate func setupSelf() {
        setupTwitter()
        setupGithub()
    }
    
    private func setupTwitter() {
        if let tweets = self.tweets {
            self.twitterDataSource = TwitterDataSource.init(with: tweets)
            self.tableView.dataSource = self.twitterDataSource
            self.tableView.delegate = self
        }
    }
    
    private func setupGithub() {
        GithubService().search(by: "swift").then { (repos) -> Void in
            self.githubDataSource = GithubDataSource.init(with: repos)
        }.catchAndShow()
    }
}

//MARK: - Actions
extension HomeViewController {
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.tableView.dataSource = self.twitterDataSource
        } else if sender.selectedSegmentIndex == 1 {
            self.tableView.dataSource = self.githubDataSource
        }
        
        self.tableView.reloadData()
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
