//
//  SocialDataSources.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import AlamofireImage

class TwitterDataSource: NSObject, UITableViewDataSource {
    var tweets:[Tweet]
    
    init(with tweets:[Tweet]) {
        self.tweets = tweets
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweet_cell", for: indexPath) as! TweetTableViewCell
        
        let tweet = self.tweets[indexPath.row]
        
        cell.bodyLabel.text = tweet.body
        cell.authorLabel.text = "By \(tweet.author)"
        
        if let url = tweet.authorImageURL {
            cell.authorImageView.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, imageTransition: .crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
        }
        
        return cell
    }
}

class GithubDataSource: NSObject, UITableViewDataSource {
    var repos:[Repo]
    
    init(with repos:[Repo]) {
        self.repos = repos
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repo_cell", for: indexPath) as! GithubTableViewCell
        
        let repo = self.repos[indexPath.row]
        
        cell.repoNameLabel.text = repo.fullName
        cell.authorLabel.text = repo.owner?.name
        
        if let updatedStirng = repo.updatedAt {
            let dateFormatter = ISO8601DateFormatter()
            dateFormatter.formatOptions = [.withFullDate]
            let date = dateFormatter.date(from: updatedStirng)!
            cell.updatedLabel.text = dateFormatter.string(from: date)
        }
        
        if let url = repo.owner?.avatarURL {
            cell.authorImageView.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, imageTransition: .crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
        }
        
        return cell
    }
}

