//
//  Extensions.swift
//  PureTextAssignment
//
//  Created by German Polyansky on 1/17/18.
//  Copyright © 2018 German Polyansky. All rights reserved.
//

import UIKit
import PromiseKit

extension Promise {
    func catchAndShow() {
        self.catch { (error) in
            let topController = UIViewController.topController
            
            var errorString:String? = nil
            
            if let err = error as? BackEndError {
                errorString = err.message
//                self.handleBackEnd(err)
            } else if let err = error as? InternalError {
                errorString = err.getErrorString()
            } else {
                errorString = error.localizedDescription
            }
            
            let alert = UIAlertController.init(title: "Error", message: errorString, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
            topController?.present(alert, animated: true, completion: nil)
        }
    }
}

extension UIViewController {
    static var topController:UIViewController? {
        get {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                
                return topController
            }
            
            return nil
        }
    }
}
